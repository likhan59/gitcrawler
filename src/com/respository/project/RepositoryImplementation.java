package com.respository.project;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.connection.database.DatabaseConnector;

import com.info.all.UserInfo;


public class RepositoryImplementation implements Repository {
	
	private DatabaseConnector connector;////////////////////////////
	

	public RepositoryImplementation() {
		
		 connector = new DatabaseConnector();
	}

	
    public void RegisterQuery(UserInfo user) {
	
	try{
		
		Connection conn = connector.getConnection();/////
		
		String query = "insert into registerdata (user_name,email,password,sex) "
				+ "values( '"
				+ user.getName()
				+ "' ,'"
				+ user.getEmail()
				+ "' , '"
				+ user.getPassword()
				+ "' , '"
				+ user.getSex()
				+ "' );";

		System.out.println(query);
		Statement statement = conn.createStatement();/////
		statement.execute(query);
		
		connector.closeConnection();
		}
	catch (SQLException e)
		{
			e.printStackTrace();
		}
    }
    
    public boolean LoginQuery(UserInfo user)
    {
    		boolean valid = false;
    		
    		try 
    		{
    			Connection conn = (Connection) connector.getConnection();
    			
    			String checkLoginQuery = "SELECT email,password FROM registerdata WHERE email ='"+ user.getEmail()+"'and password='"+user.getPassword()+"' ";
    			System.out.println(checkLoginQuery);
    			Statement statement = (Statement) conn.createStatement();
    			ResultSet result =statement.executeQuery(checkLoginQuery);
    			 
    			
    			if(result.next())		
    			{
    				String userMail= result.getString("email");
    				String userPassword=result.getString("password");
    				//System.out.println(userMail);
    				//System.out.println(userPassword);
    				if(userMail.equals(user.getEmail()) && userPassword.equals(user.getPassword()))
    				{
    					valid=true;
    				}			
    				else
    				{
    					valid=false;
    				}				
    				
    			}
    			connector.closeConnection();
    		}
    		
    		catch (SQLException e)
    		{
    			e.printStackTrace();
    		}
    		return valid;
    		
    	}


	public boolean UserNameValidation(String string) {
		
		boolean valid = false;
		
		try{
			
			Connection conn = connector.getConnection();/////
			
			String query = "select user_name from registerdata where user_name ='"+ string+"'";
            System.out.println(query);
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =statement.executeQuery(query);
			 
			
			if(result.next())		
			{
				String userMail= result.getString("user_name");
				System.out.println(userMail);
				
				if(userMail.equals(string))
				{
					System.out.println("email match "+string);
					valid=true;
				}			
				else
				{
					System.out.println("mail not match "+string);
					valid=false;
				}				
				
			}
			connector.closeConnection();
			
			}
		catch (SQLException e)
			{
				e.printStackTrace();
			}
		return valid;
	}
	
	public boolean EmailValidation(String string) {
		
		boolean valid = false;
		
		try{
			
			Connection conn = connector.getConnection();/////
			
			String query = "select email from registerdata where email ='"+ string+"'";
            System.out.println(query);
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =statement.executeQuery(query);
			 
			
			if(result.next())		
			{
				String userMail= result.getString("email");
				System.out.println(userMail);
				
				if(userMail.equals(string))
				{
					System.out.println("email match "+string);
					valid=true;
				}			
				else
				{
					System.out.println("mail not match "+string);
					valid=false;
				}				
				
			}
			connector.closeConnection();
			
			}
		catch (SQLException e)
			{
				e.printStackTrace();
			}
		return valid;
	}


}

