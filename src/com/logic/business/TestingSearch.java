package com.logic.business;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.filechooser.FileNameExtensionFilter;

import com.info.all.ResultInfo;
 
public class TestingSearch 
{
	private String userName = System.getProperty("user.name");
	private String info;
	int i=0;
	
	private ArrayList<String> resultData = new ArrayList<String>();
	
	public TestingSearch(String info) 
	{
		this.info = info;
			getResultData();
	}
	
	public ArrayList<String> getResultDataString() 
	{
		return  resultData;
	}
		
	private void read(File file) throws IOException {
		 
		if (file.isDirectory()) {
 
			// directory is empty, then delete it
			if (file.list().length == 0) {
 
			
 
			} else {
 
				String files[] = file.list();
				File fileRead = null;
				
				
			
					// construct the file structure
					for(int i=0;i<files.length;i++)
					{
						String temp = files[i];
				
						fileRead = new File(file, temp);
					
					//System.out.println("fileRead: "+fileRead);
					
					
					if(fileRead.isDirectory()){
						String result = execute("cd "+fileRead);
					    //System.out.println("cd to :"+result);
					}
			
					read(fileRead);
					
					}
					
				}
	
		
 
		} else {
			//System.out.println("qq: "+file.getName());
			// c,cs,cpp,html,htm,css,js,jsp,java,php
			if(file.getName().toString().contains(".java") || file.getName().toString().contains(".html") || file.getName().toString().contains(".css")|| file.getName().toString().contains(".jsp") || file.getName().toString().contains(".cpp") )
			{
			//System.out.println("cat "+file+" | grep -i \'"+info+"\'");
			String result = execute("cat "+file+" | grep -i \'"+info+"\'");
			
			if(!(result.equals(""))){
			CommentCutter cutter = new CommentCutter(result);
			result = cutter.getFinalResultString();
			//System.out.println("result: "+result);
			
			resultData.add(file.getPath());
			resultData.add(result);
			
			//System.out.println(file.getAbsolutePath());
			//System.out.println(result);
			
			}
			}
			
		}
	}
	private String execute(String command) {
		StringBuilder sb = new StringBuilder();
		String[] commands = new String[] { "/bin/sh", "-c", command };
		try {
			Process proc = new ProcessBuilder(commands).start();
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
 
			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					proc.getErrorStream()));
 
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
 
			while ((s = stdError.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	private void getResultData() {
		// TODO Auto-generated method stub
		long time = System.currentTimeMillis();
		File file = new File("/opt/lampp/htdocs/xampp/Projects");
      
		String result = execute("cd "+file);
		//System.out.println(result);
		try {
			read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Time taken to execute commands: "
				+ (System.currentTimeMillis() - time) + " mili seconds");
 
	}
	
}