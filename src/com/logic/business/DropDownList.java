package com.logic.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class DropDownList {
	private String name = System.getProperty("user.name");
	
	public DropDownList() {
		
	}
	
	 public String dataCommitID() {
		// TODO Auto-generated method stub
		File file = new File("/opt/lampp/htdocs/xampp/Projects");
		 
		String files[] = file.list();

			File fileSearch = new File(file, files[0]);
            String datas = execute("cd "+fileSearch+" && git log --oneline | awk '{print $1}'");
			//System.out.println("p "+datas);
			
			return datas;
		    
	}
	

	public String execute(String command) {
		StringBuilder sb = new StringBuilder();
		String[] commands = new String[] { "/bin/sh", "-c", command };
		try {
			Process proc = new ProcessBuilder(commands).start();
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
 
			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					proc.getErrorStream()));
 
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
 
			while ((s = stdError.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
/*	public static void main(String[] args) {
		
		 //new DropDownList();
		
		
	}*/
	
	public String dataCommitName() {
		File file = new File("/opt/lampp/htdocs/xampp/Projects");
 
				String files[] = file.list();
 
					File fileSearch = new File(file, files[0]);
                    String datas = execute("cd "+fileSearch+" && git log --oneline | awk '{for(i=2;i<NF;i++) printf $i \" \"; print $NF}'");
					//System.out.println("ldata: "+datas);
					
					return datas;		    
			}
}
