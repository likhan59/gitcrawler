package com.logic.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.catalina.connector.Request;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class CommitOptimiser
{
	String commitResult;
	String info;
	public String[] spiltDiff;
	public String[] outCode;
	public String author;
	//public ArrayList<String> onlyCode=new ArrayList<String>();
	
	public CommitOptimiser(String commitResult,String info) 
	{
		this.commitResult = commitResult;
		this.info = info;
		spiltDiff=createOnlyCodeList();
		
	}
	
	
	public String[] createOnlyCodeList()
	{
		//String str = req
		File file = new File("/opt/lampp/htdocs/xampp/Projects");
		String files[] = file.list();
		    
		File fileSearch = new File(file, files[0]);
		System.out.println(fileSearch);
	    author = execute("cd "+fileSearch+" && git show "+info+" | head -3");//
	    String data = execute("cd "+fileSearch+" && git show "+info+" | sed '/^+/!d;'");
	    outCode = data.split("\\+\\+\\+");
	    String header = execute("cd "+fileSearch+" && git show "+info+" | sed '/^+/!d;'| grep '+++'");
	    
	    String singleLinks[] = header.split("\\r?\\n");
	    //header = header.replace("\r" ,"");///\r\n+|\r+|\n+|\t+/i
	   // header= header.replace("/^.*[\\\/]/","");
	   // String link[]=singleLinks[0].split("\\.(?=[^\\.]+$)");
	   // System.out.println(link[0]);
		return singleLinks;
	}
	
	private String execute(String command) {
		StringBuilder sb = new StringBuilder();
		String[] commands = new String[] { "/bin/sh", "-c", command };
		try {
			Process proc = new ProcessBuilder(commands).start();
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
 
			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					proc.getErrorStream()));
 
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
 
			while ((s = stdError.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
}
