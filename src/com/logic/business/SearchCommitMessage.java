package com.logic.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class SearchCommitMessage {
	String info;
	
	public SearchCommitMessage(String info) {
		this.info=info;
	}
	
	private File cdToFile()
	{
		File file = new File("/opt/lampp/htdocs/xampp/Projects");
		 
		String files[] = file.list();

		return new File(file, files[0]);
	}
	
	private String dataCommitID() {
		// TODO Auto-generated method stub
		
            String datas = execute("cd "+cdToFile()+" && git log --oneline | awk '{print $1}'");
			
			
			return datas;
		    
	}
	
	private String dataCommitMessages() {
		// TODO Auto-generated method stub
		
            String datas = execute("cd "+cdToFile()+" && git log --oneline | awk '{for(i=2;i<NF;i++) printf $i \" \"; print $NF}'");
			
			
			return datas;
		    
	}
	
	public String getFirstCommitMsg()
	{
		int number = -5;

		String menuid[] = dataCommitID().split("\\r?\\n");
		String menudatas[] = dataCommitMessages().split("\\r?\\n");
		for(int i=menuid.length-1;i>=0;i--)
		{
			String resultFound = execute("cd "+cdToFile()+" && git show "+menuid[i]+" | grep -i -w \'"+info+"\'");
			//System.out.println("cd "+cdToFile()+" && git show "+menuid[i]+" | grep -w -i \'"+info+"\'");
			System.out.println("333333333::::::"+resultFound);
			if(resultFound!=null && resultFound.length()!=0 && resultFound.contains("+"))
			{
				number=i;
				break;
			}
		}
		//System.out.println("nn"+number);
		if(number>=0)
		{
		System.out.println("rrrrrrrrrrrrrrrrrrr :::::::"+menudatas[number]);
		return menudatas[number];
		}
		else
		{
			return "nFd";
		}
		
		
	}
	
	
	private String execute(String command) {
		StringBuilder sb = new StringBuilder();
		String[] commands = new String[] { "/bin/sh", "-c", command };
		try {
			Process proc = new ProcessBuilder(commands).start();
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
 
			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					proc.getErrorStream()));
 
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
 
			while ((s = stdError.readLine()) != null) {
				sb.append(s);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
