package com.servlet.url;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.logic.business.TestingSearch;

/**
 * Servlet implementation class ServletSearch
 */
@WebServlet("/ServletSearch")
public class ServletSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String info = request.getParameter("message");
		System.out.println("lolo::::::::::: "+info);
		
		TestingSearch test = new TestingSearch(info);
		test.getResultDataString();
		PrintWriter print = response.getWriter();
		for(int i=0;i<test.getResultDataString().size();i++)
		{
			if(i%2==0)
			print.println(test.getResultDataString().get(i).toString()+"^");
			else
			print.println(test.getResultDataString().get(i).toString()+"~");
		}
		
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		
	}
	
	
}
