package com.servlet.url;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.logic.business.CommentCutter;
import com.logic.business.CommitData;
import com.logic.business.CommitOptimiser;

/**
 * Servlet implementation class ServletCommi
 */
@WebServlet("/ServletCommit")
public class ServletCommit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public ServletCommit() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String[] path;
		String[] code;
		String info = request.getParameter("message");
		
		CommitData commit = new CommitData();
		//System.out.println("zzzzzzzzzzzzzzzzzzzzzzzz");
		String commitData = commit.getCommitInfo(info);
		CommitOptimiser optimiser = new CommitOptimiser(commitData,info); 
		path = optimiser.spiltDiff;
		code = optimiser.outCode;
		CommentCutter cutter = new CommentCutter(commitData);
		//commitData = cutter.HTMLtagRemover(commitData);
		PrintWriter print = response.getWriter();
		//for(int i=0;i<path.length;i++)
		//print.println(cutter.HTMLtagRemover(path[i]));
		print.println(optimiser.author);
		if(path.length!=0){
		for(int i=0;i<path.length;i++)
		{
			print.println("<a href=#"+i+">"+path[i]+"</a>");
		}
		System.out.println(code.length+"   "+path.length);
		}
		if(code.length!=0){
		for(int i=0; i<code.length;i++)
		{
			print.println("<div id="+i+">"+cutter.HTMLtagRemover(code[i])+"<div><br><br><br>");
		}
		}
		//System.out.println(commitData);
	}
}
