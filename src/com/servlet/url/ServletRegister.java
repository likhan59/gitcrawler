package com.servlet.url;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.SeekableByteChannel;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.connection.database.DatabaseConnector;
import com.info.all.UserInfo;
import com.logic.business.CreateUserDirectory;
import com.respository.project.RepositoryImplementation;

/**
 * Servlet implementation class ServletRegister
 */
@SuppressWarnings("unused")
@WebServlet("/ServletRegister")
public class ServletRegister extends HttpServlet {
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String name, password, sex, email;

		UserInfo user = new UserInfo();
		PrintWriter print = response.getWriter();
		user.setName(request.getParameter("name"));
		user.setEmail(request.getParameter("email"));
		user.setSex(request.getParameter("sex"));
		if (request.getParameter("password").equals(
				request.getParameter("cpassword"))) {

			user.setPassword(request.getParameter("password"));
			RepositoryImplementation insertintoDB = new RepositoryImplementation();
			insertintoDB.RegisterQuery(user);
			String directoryName = request.getParameter("name");
			CreateUserDirectory userDirectory = new CreateUserDirectory(directoryName);
			HttpSession session = request.getSession(true);
			session.setAttribute("currentSessionUser",
					request.getParameter("email"));
			response.sendRedirect("CrawlPage.jsp");
		} else {
			response.sendRedirect("join.jsp");
		}

	}

}
