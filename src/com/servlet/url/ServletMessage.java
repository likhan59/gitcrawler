package com.servlet.url;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.logic.business.SearchCommitMessage;

/**
 * Servlet implementation class ServletMessage
 */
@WebServlet("/ServletMessage")
public class ServletMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletMessage() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter print = response.getWriter();
		String info = request.getParameter("message");
		SearchCommitMessage message = new SearchCommitMessage(info);
		String FirstCommit = message.getFirstCommitMsg();
		print.println(FirstCommit);
	}

}
