<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home Page</title>
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<%@include file="Header.jsp"%>

<div class="info" id="gitInfo">
<a href="http://www.vogella.com/articles/Git/article.html#git_definition" target="_blank">Git</a> is a Version control Management system allows
you to track the history of a collection of files and includes the functionality
to revert the collection of files to another version. Each version captures a 
snapshot of the file system at a certain point in time. The collection of files 
is usually source code for a programming language but it can put any type of file
under version control.
<p>Git originates from the Linux kernel development and is used by many popular Open Source projects, as well as by many commercial organizations.</p>
</div>

<div class="info" id="gitCrawlerInfo">
Git Crawler is a fast and usable source code search and cross reference engine. 
It helps you search, cross-reference and navigate your source tree. It can 
understand various program file formats and version control histories of Git. 
For now it can only clone projects of github and BitBucket. It provides Full 
text search and commits search facility to the developers. It also provides facility
to see the source for every search results. It will show the line number and path 
for every found match. It also gives a user friendly and interactive interface.
</div>
<%@include file="Footer.jsp"%>
</body>
</html>