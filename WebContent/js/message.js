var info;
$(document).ready(function(){
	
	$("#submitButton").click(function(){
		
		$("#resultDiv").empty();
		$("h1").css("margin-top","2%");
		info = $("#s").val();
		sendCommentData(info);
		document.getElementById("resultDiv").style.visibility="visible";
		
	});
	
	$("#s").keypress(function(e)
	{
		if(e.which===13)
		{
			e.preventDefault(); 
			$("#resultDiv").empty();
			info = $("#s").val();
			sendCommentData(info);
			document.getElementById("resultDiv").style.visibility="visible";
			
		}
		
	});
	
	function sendCommentData(CommentDataString) 
	{
		$.ajax({
			type : "post",
			url : "ServletMessage",
			data:{"message":CommentDataString},
		    success: function (totalData) {
		    	
		    	
		    	var parentDiv=document.getElementById("resultDiv");
		    	
		    		
		    		
		    		var childDiv=document.createElement('div');
		    		
		    		childDiv['class']='resultDivs';
		    		
		    		var childAnchor=document.createElement('a');
//		    		childAnchor['id'] = "a"+i;
//		    		childAnchor['class']='resultLinks';
//		    		childAnchor['target']="_blank";
//		    		var contentLinks=mainData[0];
//		    		
//		    		if(contentLinks.length!=0 && mainData[1].indexOf("unexpected") == -1){
//		    			
//		    		contentLinks= contentLinks.replace(/\r\n+|\r+|\n+|\t+/i , '');
//		    		
//		    		var filename = contentLinks.replace(/^.*[\\\/]/, '');
		    		
		    //		childAnchor['href'] ="GoToSource.jsp?id="+mainData[0]+"&f="+info;
		    		if(totalData.match(/nFd/))
		    		{
		    			childDiv.innerHTML="<br/><p style='background-color: #ACB6B6;alignment:center;'>No Results Found</p><br/>";
		    			childDiv.appendChild(childAnchor);
			    		parentDiv.appendChild(childDiv);
		    		}
		    		else	
		    		{
		    			childAnchor.innerHTML="<br><br><pre style='overflow:auto; background-color: #ACB6B6;'>"+totalData+"</pre><br/><br/>";
			    		childDiv.appendChild(childAnchor);
			    		parentDiv.appendChild(childDiv);
		    			
		    		}
		     }
		   
	    });
	}
});
