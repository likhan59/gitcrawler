<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Join page</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript" src="js/external.js"></script>
</head>
<body>
	<%@include file="Header.jsp"%>
	
	<div id="JoinForm">
		<form action="ServletRegister" method="post" id="Form">
			
			Name : <br /> <input type="text" class="design" name="name"
				placeholder="enter your name" id="field_name" required><span id="Right_Wrong"></span> <br>
			<br /> Email :<br /> <input type="email" name="email" class="design"
				placeholder="enter your email" id="field_email" required> <span id="emailRightWrong"></span><br />
			<br /> Password :<br /> <input type="password" class="design"
				name="password" maxlength="20" placeholder="enter your password"
				required><br>
			<br> Retype Password :<br> <input type="password"
				class="design" name="cpassword" maxlength="20"
				placeholder="again your password" required><br />
			<br /> Sex :<br />
			<div class="middle">
				<input type="radio" name="sex" value="Male"> Male<br /> <input
					type="radio" name="sex" value="Female"> Female <br /> <br />
			</div>
			<input type="submit" name="submit" id="submit"><br />
		</form>
	</div>

	<%@include file="Footer.jsp"%>
	
</body>
</html>