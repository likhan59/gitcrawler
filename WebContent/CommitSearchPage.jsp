<%@page import="javax.swing.text.StyledEditorKit.BoldAction"%>
<%@page import="com.sun.swing.internal.plaf.metal.resources.metal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.logic.business.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Commit Search Page</title>
<link rel="stylesheet" type="text/css" href="css/search.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="js/commit.js"></script> 
</head>
<body>
<%@include file="SearchChoice.jsp"%>
<%
		Cloned fileExist =  new Cloned();
		boolean exist = fileExist.getFile();
		if(exist){
	%>
<div id="page">
<h1>Git Crawler</h1>
<%
  ArrayList<String> menuData = new ArrayList<String>();
   ArrayList<String> menuId = new ArrayList<String>();

  DropDownList lists = new DropDownList();
  String menudatas = lists.dataCommitName(); 
  String id = lists.dataCommitID(); 
  
  String menuid[] = id.split("\\r?\\n");
  String menulistitems[] = menudatas.split("\\r?\\n");
 
%>
<select id="commitSearch" style='width: 70%;'>
<%
  for(int i=0;i<menuid.length;i++)
  {
 %>
  <option value="<%=menuid[i]%>"><%=menulistitems[i]%></option>
 <%
  }
 %>
</select>
<!-- <input type="button" value="Search" name="searchResult" id="submitCommitButton"/> -->
<input type="button" value="Search" name="searchResult" id="submitCommitButton"/>
</div>

<div id="resultCommitDiv">
		<!-- <div id="SingleResult">
		</div> -->
	</div>
	<%
		}
		else
		{
			response.sendRedirect("CrawlPage.jsp");
		}
	%>
	
		<%@include file="Footer.jsp"%>
	
</body>
</html>