<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Crawl Page</title>

<!-- <link rel="stylesheet" type="text/css" href="css/style.css" /> -->
<link rel="stylesheet" type="text/css" href="css/search.css" />

<script type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript" src="js/external.js"></script>
</head>
<body>

	<%@include file="SearchChoice.jsp"%>
	
	
	<div id="page">
		
		<h1>Git Crawler</h1>
		

		<form id="searchForm" action="ServletURL" method="post">
		
				<input type="text" name="input_url"
				placeholder="Enter your project cloning url" id="url_design">
				<input type="submit" name="submit_url" value="submit" id="button_design" onclick="hello()">	 	
				
		</form>
	</div> 

	<div id="dvLoading" ></div>
	
	<%@include file="Footer.jsp"%>

</body>
</html>